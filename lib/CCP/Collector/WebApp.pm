package CCP::Collector::WebApp;
# ABSTRACT: web app for CCP Collector Service

=head1 DESCRIPTION

L<Dancer>-based web app for the CCP Collector Service.

=cut

use strict;
use warnings;
use Dancer ':syntax';

use CCP::Collector v1.0.0 ();
use CCP::Collector::Error::BadKey ();
use CCP::Collector::Error::BadSecret ();
use TryCatch 1.001000; # for "catch" fix

set 'show_errors' => 1;

my $Collector = CCP::Collector->new;

use constant POST_ONLY => sub {
    status 405;
    return 'only POST requests are permitted to this address';
};

any '/' => sub {
    return 'CCP Collection Platform';
};

post '/v1/get-device-key' => sub {
    try {
        return $Collector->register_device( param('secret') );
    }
    catch (CCP::Collector::Error::BadSecret $e) {
        status 403;
        return 'bad authentication';
    }

};
any '/v1/get-device-key' => POST_ONLY;

post '/v1/store' => sub {
    if (
        not defined param 'key'
        or not defined param 'data'
    ) {
        status 400;
        return 'bad parameters';
    }
    try {
        $Collector->store(param('key'), param('data'));
    }
    catch (CCP::Collector::Error::BadKey $e) {
        status 403;
        return 'bad authentication';
    }
    return 'OK';
};
any '/v1/store' => POST_ONLY;

true;
