# NAME

CCP::Collector::WebApp - web app for CCP Collector Service

# VERSION

This module is part of distribution CCP-Collector-WebApp v1.1.1.

This distribution's version numbering follows the conventions defined at [semver.org](http://semver.org/).

# DESCRIPTION

[Dancer](https://metacpan.org/pod/Dancer)-based web app for the CCP Collector Service.

# SUPPORT

## Bugs / Feature Requests

Please report any bugs or feature requests by email to `bug-ccp-collector-webapp at rt.cpan.org`, or through
the web interface at [http://rt.cpan.org/NoAuth/ReportBug.html?Queue=CCP-Collector-WebApp](http://rt.cpan.org/NoAuth/ReportBug.html?Queue=CCP-Collector-WebApp). You will be automatically notified of any
progress on the request by the system.

## Source Code

The source code for this distribution is available online in a [Git](http://git-scm.com/) repository.  Please feel welcome to contribute patches.

[https://github.com/xxx/xxx](https://github.com/xxx/xxx)

    git clone git://github.com/xxx/xxx

# AUTHOR

Alex Peters <s3105178@student.rmit.edu.au>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2014 by Alex Peters.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

The full text of the license can be found in the
`LICENSE` file included with this distribution.
