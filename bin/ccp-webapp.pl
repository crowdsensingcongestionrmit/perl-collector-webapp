#!/usr/bin/env perl
# PODNAME: ccp-webapp.pl

=for :stopwords
ccp webapp

=head1 DESCRIPTION

Starts the web app.

=cut

use Dancer;
use CCP::Collector::WebApp;
dance;
